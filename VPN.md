The original setup guide was missing so here's my take:

First, follow the setup guide as seen in the README.md. Then, in your newly created proxy VM:
1. Put your .ovpn files in `/rw/config/qtunnel`
    - .ovpn files can be obtained through your VPN provider
2. Run `/usr/lib/qubes-setup --config`
3. `cd /rw/config/qtunnel` then `cp your.ovpn qtunnel.conf`
4. Restart the daemon (or the VM) `systemctl restart qubes-tunnel`
    - There is sometimes a `condition failed` error when starting up the service: `ConditionPathExistsGlob=/var/run/qubes-service/qubes-tunnel* was not met` Simply run `sudo touch /var/run/qubes-service/qubes-tunnel` followed by `systemctl restart qubes-tunnel`

You should see a notification that says "**Ready to Link**" or "**LINK UP**" if successful. If you get a constant "**Ready to Link**" message, diagnose the error using `/usr/lib/qubes-connect test-up` or `systemctl status qubes-tunnel`. 
